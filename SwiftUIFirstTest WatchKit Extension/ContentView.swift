//
//  ContentView.swift
//  SwiftUIFirstTest WatchKit Extension
//
//  Created by Joseph Lord on 05/06/2019.
//  Copyright © 2019 Joseph Lord. All rights reserved.
//

import SwiftUI
import Combine

class Model : BindableObject{
    let didChange = PassthroughSubject<Model, Never>()
    
    init(message: String) {
        self.message = message
    }
    private (set) var message: String {
        didSet {
            didChange.send(self)
        }
    }
    
    private var tapCount: Int = 0
    func increment() {
        tapCount += 1
        message = "Button tapped \(tapCount)"
    }
}

struct ContentView : View {
    
    @ObjectBinding var model: Model

    var body: some View {
        
        HStack {
            Text(model.message)
                .font(.largeTitle)
                .lineLimit(5)
            
            Button(action: {
                self.model.increment()
            }, label: {
                Image(systemName: "hare")
                    .scaledToFill()
                    .font(.largeTitle)
                    .rotationEffect(.degrees(90))
                
            })
                .frame(width: 38.0, height: 44.0)
            
            }.padding()
        
        
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView(model: Model(message: "Initial Message"))
//            .environmentObject(Model(message: "Initial Message"))
    }
}
#endif
