//
//  HostingController.swift
//  SwiftUIFirstTest WatchKit Extension
//
//  Created by Joseph Lord on 05/06/2019.
//  Copyright © 2019 Joseph Lord. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController : WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView(model: Model(message: "Watch Initial"))
           // .environmentObject(Model(message: "Watch Initial"))
    }
}
