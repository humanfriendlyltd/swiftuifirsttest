//
//  NotificationView.swift
//  SwiftUIFirstTest WatchKit Extension
//
//  Created by Joseph Lord on 05/06/2019.
//  Copyright © 2019 Joseph Lord. All rights reserved.
//

import SwiftUI

struct NotificationView : View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct NotificationView_Previews : PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
#endif
