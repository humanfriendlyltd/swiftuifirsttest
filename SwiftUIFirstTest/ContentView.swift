//
//  ContentView.swift
//  SwiftUIFirstTest
//
//  Created by Joseph Lord on 05/06/2019.
//  Copyright © 2019 Joseph Lord. All rights reserved.
//

import SwiftUI
import Combine

class Model : BindableObject{
    let didChange = PassthroughSubject<Model, Never>()
    
    init(message: String) {
        self.message = message
    }
    private (set) var message: String {
        didSet {
            didChange.send(self)
        }
    }
    
    private var tapCount: Int = 0
    func increment() {
        tapCount += 1
        message = "Button tapped \(tapCount)"
    }
}

struct ContentView : View {

    @EnvironmentObject var model: Model
    @State var count: Int = 0
    @State var text: String = "dlkfjsad"
    var tapCount = 0
    var body: some View {
        VStack {
        HStack {
            Text(model.message)
                .font(Font.system(size: 16))
            
            Button(action: {
               // self.count = count + 1
                self.model.increment()
            }, label: {
                Image(systemName: "hare")
                    .font(Font.system(size: 30))
                    .rotationEffect(.degrees(90))
                })
                    .frame(width: 38.0, height: 44.0)
            
        }.padding()
            Spacer()
            HStack {
                TextField($text)
                
            }
            Spacer()
        }
   
        
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(Model(message: "Initial Message"))
            .previewLayout(.sizeThatFits)
    }
}
#endif
